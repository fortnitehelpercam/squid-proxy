#!/bin/bash
############################################################
# Squid Proxy Installer
############################################################

if [ `whoami` != root ]; then
	echo "Lỗi: Bạn cần chạy scripts với tư cách người dùng root hoặc thêm sudo trước lệnh."
	exit 1
fi

## Lấy danh sách các IP hiện có
readarray -t IP4 <<< $((/sbin/ip -4 -o addr show scope global | awk '{gsub(/\/.*/,"",$4); print $4}') | sed 's/ /\n/g')
readarray -t IP6 <<< $((/sbin/ip -6 -o addr show scope global | awk '{gsub(/\/.*/,"",$4); print $4}') | sed 's/ /\n/g')

if [ -z "$IP6" ]
then
    echo "Server chưa có IPv6, vui lòng liên hệ support@vinahost.vn hoặc truy cập https://livechat.vinahost.vn/chat.php để được hỗ trợ"
else
    /usr/bin/wget --no-check-certificate -O /usr/local/bin/find-os https://gitlab.com/hungmv/squid-proxy/-/rawgitlab.com/hungmv/squid-proxy/-/raw/master/find-os.sh > /dev/null 2>&1
    chmod 755 /usr/local/bin/find-os

    /usr/bin/wget --no-check-certificate -O /usr/local/bin/squid-uninstall https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid-uninstall.sh > /dev/null 2>&1
    chmod 755 /usr/local/bin/squid-uninstall

    /usr/bin/wget --no-check-certificate -O /usr/local/bin/squid-add-user https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid-add-user.sh > /dev/null 2>&1
    chmod 755 /usr/local/bin/squid-add-user


    if [[ -d /etc/squid/ || -d /etc/squid3/ ]]; then
        echo "Squid Proxy already installed. If you want to reinstall, first uninstall squid proxy by running command: squid-uninstall"
        exit 1
    fi

    if cat /etc/os-release | grep PRETTY_NAME | grep "Ubuntu 22.04"; then
        /usr/bin/apt update
        /usr/bin/apt -y install apache2-utils squid
        touch /etc/squid/passwd
        mv /etc/squid/squid.conf /etc/squid/squid.conf.bak
        /usr/bin/touch /etc/squid/blacklist.acl
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/ubuntu-2204.conf
        if [ -f /sbin/iptables ]; then
            /sbin/iptables -I INPUT -p tcp --dport 3128 -j ACCEPT
            /sbin/iptables-save
        fi
        service squid restart
        systemctl enable squid
    elif cat /etc/os-release | grep PRETTY_NAME | grep "Ubuntu 20.04"; then
        /usr/bin/apt update
        /usr/bin/apt -y install apache2-utils squid
        touch /etc/squid/passwd
        /bin/rm -f /etc/squid/squid.conf
        /usr/bin/touch /etc/squid/blacklist.acl
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid.conf
        if [ -f /sbin/iptables ]; then
            /sbin/iptables -I INPUT -p tcp --dport 3128 -j ACCEPT
            /sbin/iptables-save
        fi
        service squid restart
        systemctl enable squid
    elif cat /etc/os-release | grep PRETTY_NAME | grep "Ubuntu 18.04"; then
        /usr/bin/apt update
        /usr/bin/apt -y install apache2-utils squid3
        touch /etc/squid/passwd
        /bin/rm -f /etc/squid/squid.conf
        /usr/bin/touch /etc/squid/blacklist.acl
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid.conf
        /sbin/iptables -I INPUT -p tcp --dport 3128 -j ACCEPT
        /sbin/iptables-save
        service squid restart
        systemctl enable squid

    elif cat /etc/os-release | grep PRETTY_NAME | grep "jessie"; then
        # OS = Debian 8
        /bin/rm -rf /etc/squid
        /usr/bin/apt update
        /usr/bin/apt -y install apache2-utils squid3
        touch /etc/squid3/passwd
        /bin/rm -f /etc/squid3/squid.conf
        /usr/bin/touch /etc/squid3/blacklist.acl
        /usr/bin/wget --no-check-certificate -O /etc/squid3/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid.conf
        /sbin/iptables -I INPUT -p tcp --dport 3128 -j ACCEPT
        /sbin/iptables-save
        service squid3 restart
        update-rc.d squid3 defaults
        ln -s /etc/squid3 /etc/squid
    elif cat /etc/os-release | grep PRETTY_NAME | grep "stretch"; then
        # OS = Debian 9
        /bin/rm -rf /etc/squid
        /usr/bin/apt update
        /usr/bin/apt -y install apache2-utils squid
        touch /etc/squid/passwd
        /bin/rm -f /etc/squid/squid.conf
        /usr/bin/touch /etc/squid/blacklist.acl
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid.conf
        /sbin/iptables -I INPUT -p tcp --dport 3128 -j ACCEPT
        /sbin/iptables-save
        systemctl enable squid
        systemctl restart squid
    elif cat /etc/os-release | grep PRETTY_NAME | grep "buster"; then
        # OS = Debian 10
        /bin/rm -rf /etc/squid
        /usr/bin/apt update
        /usr/bin/apt -y install apache2-utils squid
        touch /etc/squid/passwd
        /bin/rm -f /etc/squid/squid.conf
        /usr/bin/touch /etc/squid/blacklist.acl
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid.conf
        /sbin/iptables -I INPUT -p tcp --dport 3128 -j ACCEPT
        /sbin/iptables-save
        systemctl enable squid
        systemctl restart squid
    elif cat /etc/os-release | grep PRETTY_NAME | grep "CentOS Linux 7"; then
        yum install squid httpd-tools -y
        /bin/rm -f /etc/squid/squid.conf
        /usr/bin/touch /etc/squid/blacklist.acl
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/squid-centos7.conf
        systemctl enable squid
        systemctl restart squid
        #firewall-cmd --zone=public --permanent --add-port=3128/tcp
        #firewall-cmd --reload
    elif cat /etc/os-release | grep PRETTY_NAME | grep "CentOS Linux 8"; then
        yum install squid httpd-tools -y
        /bin/rm -f /etc/squid/squid.conf
        /usr/bin/touch /etc/squid/blacklist.acl
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/squid-centos7.conf
        systemctl enable squid
        systemctl restart squid
        # firewall-cmd --zone=public --permanent --add-port=3128/tcp
        # firewall-cmd --reload
    else
        echo "Hệ điều hành không được hỗ trợ.\n"
        echo ""
        exit 1;
    fi

    echo "Cấu hình Squid với IPv6"

    # cấu hình với IPv6
    SQUID_CONFIG="\n"
    for IP_ADDR in ${IP6[@]}; do
        ACL_NAME="proxy_ip_${IP_ADDR//\./_}"
        SQUID_CONFIG+="acl ${ACL_NAME}  myip ${IP_ADDR}\n"
        SQUID_CONFIG+="tcp_outgoing_address ${IP4} ${ACL_NAME}\n\n"
    done

    echo -e $SQUID_CONFIG >> /etc/squid/squid.conf

    echo "Khởi động lại squid..."

    systemctl restart squid

    ## Tạo User
    USERNAME=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 8 ; echo '')
    PASSWORD=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 12 ; echo '')
    echo "$USERNAME    $PASSWORD" >> user.txt
    /usr/bin/htpasswd -b -c /etc/squid/passwd USERNAME PASSWORD


    echo "Thông tin đăng nhập proxy:"
    echo "username: $USERNAME password: $PASSWORD"
    echo "Xem lại tài khoản mật khẩu tại tệp user.txt"
    echo 
    echo "Để tạo thêm người dùng proxy, hãy chạy lệnh: squid-add-user"
    echo "Xem thêm hướng dẫn tại https://gitlab.com/hungmv/squid-proxy"
fi

echo 
