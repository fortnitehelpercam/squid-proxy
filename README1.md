# Scrip tự động cài đặt proxy Squid trên 3 hệ điều hành linux sau:
* Ubuntu 18.04, 20.04, 22.04
* Debian 8, 9, 10
* CentOS 7, 8

## Cài đặt:

Để cài đặt, hãy chạy lệnh sau với Superuser:
```
curl -O https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid3-install.sh
sudo bash squid3-install.sh
```

## Tạo user

Để tạo user, chạy lệnh
```
squid-add-user
```
hoặc lệnh sau

```
sudo /usr/bin/htpasswd -b -c /etc/squid/passwd USERNAME PASSWORD
```
Để cập nhật mật khẩu cho người dùng hiện tại, chạy
```
sudo /usr/bin/htpasswd /etc/squid/passwd USERNAME
```
thay thế USERNAME và PASSWORD bằng tên người dùng và mật khẩu bạn muốn đặt.

Khởi động lại squid proxy
```
sudo systemctl reload squid
```
## Thay đổi cổng squid proxy
Bạn có thể sử dụng lệnh sed để thay thế số cổng
```
sed -i 's/^http_port.*$/http_port NEW_PORT_HERE/g'  /etc/squid/squid.conf
```

Hoặc chỉnh sửa tệp cấu hình Squid bằng trình chỉnh sửa vi hoặc nano
```
vi /etc/squid/squid.conf
```
Trong tệp, tìm phần http_port như bên dưới
```
http_port NEW_PORT_HERE
```
Trong hai lệnh trên, thay thế NEW_PORT_HERE bằng số cổng bạn cần.
Ví dụ: để chạy squid proxy trên cổng 3333, hãy chạy
```
sed -i 's/^http_port.*$/http_port 3333/g'  /etc/squid/squid.conf
```

Sau khi thay đổi cổng, bạn cần khởi động lại squid proxy
```
sudo systemctl restart squid
```
Nếu bạn có tường lửa, bạn cần mở cổng trong tường lửa.
Ví dụ với tường lửa UFW của ubuntu
```
sudo ufw allow 3333/tcp
sudo ufw reload
```
___Thay thế 3333 thành số cổng squid proxy của bạn.___
## Cấu hình squid proxy với IPv6
LƯU Ý: Điều này chỉ cần thiết nếu bạn đã có IPv6 trên máy chủ của mình.

```
curl -O https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid-conf-ipv6.sh
sudo bash squid-conf-ipv6.sh
```
Khởi động lại squid proxy
```
sudo systemctl restart squid
```
