#!/bin/bash
# Author: hungmv@vinahost.vn

## Lấy danh sách các IP hiện có
readarray -t IP4S <<< $((/sbin/ip -4 -o addr show scope global | awk '{gsub(/\/.*/,"",$4); print $4}') | sed 's/ /\n/g')
readarray -t IP6S <<< $((/sbin/ip -6 -o addr show scope global | awk '{gsub(/\/.*/,"",$4); print $4}') | sed 's/ /\n/g')
## Lấy danh sách các user
readarray -t USERS <<< $(cat /etc/squid/passwd | cut -d ':' -f1)

NUM_IP6S=${#IP6S[@]}
NUM_USERS=${#USERS[@]}

SQUID_CONFIG="\n"
for (( i=0; i<$NUM_IP6; i++ )); do
    if (( $i >= $NUM_USERS )); then
        break
    fi

    ACL_NAME="${USERS[$i]}"
    SQUID_CONFIG+="acl ${ACL_NAME}  myip ${IP6S[$i]}\n"
    SQUID_CONFIG+="tcp_outgoing_address ${IP4} ${ACL_NAME}\n\n"
done

echo "Updating squid config"

echo -e $SQUID_CONFIG >> /etc/squid/squid.conf

echo "Restarting squid..."

systemctl restart squid

echo "Done"